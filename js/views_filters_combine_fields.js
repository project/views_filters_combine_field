(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.views_filters_combine_field = {
    attach: function (context) {
      let form_id = drupalSettings.views_filters_combine_field.form_exposed_id;
      let select_id =
        drupalSettings.views_filters_combine_field.select_field_id[1];
      let search_field_id =
        drupalSettings.views_filters_combine_field.search_field_id[0];
      let operator = drupalSettings.views_filters_combine_field.operator;
      let current_values =
        drupalSettings.views_filters_combine_field.current_value;
      let operator_field_id =
        drupalSettings.views_filters_combine_field.operator_field[0];
      let current_operator =
        drupalSettings.views_filters_combine_field.current_operator;
      $(`.${select_id}`).on("change", function () {
        setTimeout(change(), 2500);
      });
      function change() {
        var firstDropVal = $(`.${select_id}`).val();
        firstDropVal = $(`option[value="${firstDropVal}"]`)
          .text()
          .replaceAll(" ", "_")
          .toLowerCase();
        var arrayobj = drupalSettings.views_filters_combine_field.elements;
        let type = arrayobj[firstDropVal]["#type"];
        let options = arrayobj[firstDropVal]["#options"];
        let selectoperator =
          drupalSettings.views_filters_combine_field.operator.select;
        let name = $(`.${search_field_id}`).attr("name");
        if (type == "select") {
          $(`.${search_field_id}`).replaceWith(
            `<select class="change-drop ${search_field_id} form-select form-element" id="edit-combinefield" name='${name}' data-drupal-selector="edit-combinefield"> </select>`
          );
          $(`.${search_field_id}`).fadeOut(50).fadeIn(1000);
          var select = $(`.${search_field_id}`);
          for (var i in options) {
            var opt = options[i];
            var el = document.createElement("option");
            el.textContent = opt;
            el.value = i;
            select.append(el);
          }
          $(`.${operator_field_id} option`).each(function () {
            $(this).remove();
          });
          var select = $(`.${operator_field_id}`);
          for (var i in selectoperator) {
            var opt = selectoperator[i];
            var el = document.createElement("option");
            el.textContent = opt;
            el.value = i;
            select.append(el);
          }
          $(`.${operator_field_id} option[value="${current_operator}"]`).attr(
            "selected",
            "selected"
          );
          $(`.${search_field_id} option[value="${current_values}"]`).attr(
            "selected",
            "selected"
          );
        } else if (type == "number") {
          let textoperator =
            drupalSettings.views_filters_combine_field.operator.number;
          $(`.${operator_field_id} option`).each(function () {
            $(this).remove();
          });
          var select = $(`.${operator_field_id}`);
          for (var i in textoperator) {
            var opt = textoperator[i];
            var el = document.createElement("option");
            el.textContent = opt;
            el.value = i;
            select.append(el);
          }
          $(`.${operator_field_id} option[value="${current_operator}"]`).attr(
            "selected",
            "selected"
          );
          $( `.${search_field_id}` ).replaceWith(
            `<input class="change-drop form-text ${search_field_id}" id='edit-combinefield' name='${name}' data-drupal-selector="edit-combinefield" type="text" value="${current_values}">`
          );
          $(`.${search_field_id}`).fadeOut(50).fadeIn(1000);
        } else {
          let textoperator =
            drupalSettings.views_filters_combine_field.operator.textfield;
          $(`.${operator_field_id} option`).each(function () {
            $(this).remove();
          });
          var select = $(`.${operator_field_id}`);
          for (var i in textoperator) {
            var opt = textoperator[i];
            var el = document.createElement("option");
            el.textContent = opt;
            el.value = i;
            select.append(el);
          }
          $(`.${operator_field_id} option[value="${current_operator}"]`).attr(
            "selected",
            "selected"
          );
          $(`.${search_field_id}`).replaceWith(
            `<input class="change-drop form-text ${search_field_id} form-text form-element" id='edit-combinefield' name='${name}' data-drupal-selector="edit-combinefield" type="text" value="${current_values}">`
          );
          $(`.${search_field_id}`).fadeOut(50).fadeIn(1000);
        }
      }
      change();
    },
  };
})(jQuery, Drupal, drupalSettings);
